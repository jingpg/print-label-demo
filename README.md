[TOC]

# 1.前言

## 1.1打印机型号及驱动说明

打印机型号：佳博GP-3120TU打印机

标签纸规格：40mm*30mm 间隔2mm

驱动：佳博条码打印机通用驱动

TSCLIB.dll函数库支持：基本支持佳博的所有打印机



## 1.2背景概要

因为公司的业务需要，需要用java程序批量打印小标签，小标签是热敏打印机打出来可以贴的那种标签，就像超市的菜包上面贴的价格规格标签。先看一下我最终的效果图：

![41582893814_.pic_hd](https://tva1.sinaimg.cn/large/00831rSTgy1gcchvh2htdj30vy0kiwgo.jpg)

为了对接这个打印机，花费了整整一天的时间走过了他所有的坑，所以在此记录下走坑的过程

# 2.填坑

1. 官方提供的DEMO使用JDK1.7，本人使用JDK1.8没有发现问题，目测支持1.7以上版本

2. 官方提供TSCLIB.dll(核心动态函数库)的32位和64位版本，**划重点：<u>==事实上这个位数相当重要，TSCLIB.dll必须与jdk的位数相同==</u>**

3. **如果打印内容中包含中文，代码则以GBK编码编写，且编译成class文件也是用GBK编码，否则打印标签的中文会乱码**

4. 使用JNA框架调用dll库，JNA请使用3.2.5版本，maven依赖如下，我第一次使用4.0.0版本JNA读取dll，出现异常无法正常读取

    ```xml
    <dependency>
        <groupId>net.java.dev.jna</groupId>
        <artifactId>jna</artifactId>
        <version>3.2.5</version>
    </dependency>
    ```
    

5. 计算机上需要安装打印机驱动，我使用的佳博打印机通用驱动



# 3.上代码

demo代码很简单，是一个简单的java项目.
先上截图，后上代码，文档和TSCLIB.dll在附件中请下载支持一波


## 1.项目截图

![image-20200228221414125](https://tva1.sinaimg.cn/large/00831rSTgy1gcch2uaepkj30ga0ds75n.jpg)
    
TSCLIB.dll文件直接放在项目的根路径下，==注意：不是classpath，是项目跟路径==


## 2.==中文打印需要注意两点==

- .java源文件切记使用GBK编码
  

![image-20200228221547526](https://tva1.sinaimg.cn/large/00831rSTgy1gcch4evmiej31bw0u0h3l.jpg)




- 编译成.class文件请使用GBK编码，idea启动时添加VM arguments 加 -Dfile.encoding=GBK即可
  

![image-20200228221726764](https://tva1.sinaimg.cn/large/00831rSTgy1gcch64lpw0j31oy0u0dmm.jpg)


​    

## 3.项目源码

```java
public class PrintLabelMain {
        public interface TscLibDll extends Library {
            // 此处默认读取项目根路径下的TSCLIB.dll，也可以填写绝对路径
            TscLibDll INSTANCE = (TscLibDll) Native.loadLibrary ("TSCLIB", TscLibDll.class);
    
            // 以下为dll函数库支持的方法，方法的作用与参数说明见附件【dll函数库api文档】
            int about ();
            int openport (String pirnterName);
            int closeport ();
            int sendcommand (String printerCommand);
            int setup (String width,String height,String speed,String density,String sensor,String vertical,String offset);
            int downloadpcx (String filename,String image_name);
            int barcode (String x,String y,String type,String height,String readable,String rotation,String narrow,String wide,String code);
            int printerfont (String x,String y,String fonttype,String rotation,String xmul,String ymul,String text);
            int clearbuffer ();
            int printlabel (String set, String copy);
            int formfeed ();
            int nobackfeed ();
            int windowsfont (int x, int y, int fontheight, int rotation, int fontstyle, int fontunderline, String szFaceName, String content);
        }
    
        public static void main(String[] args) {
            TscLibDll.INSTANCE.about();
            TscLibDll.INSTANCE.openport("TSC TTP-2410M");
            TscLibDll.INSTANCE.sendcommand("REM ***** This is a test by JAVA. *****");
            TscLibDll.INSTANCE.setup("100", "100", "5", "8", "0", "0", "0");
            TscLibDll.INSTANCE.clearbuffer();
            TscLibDll.INSTANCE.sendcommand("PUTPCX 550,10,\"UL.PCX\"");
            TscLibDll.INSTANCE.printerfont ("100", "10", "3", "0", "1", "1", "(JAVA) DLL Test!!");
            TscLibDll.INSTANCE.barcode("100", "40", "128", "50", "1", "0", "2", "2", "123456789");
            TscLibDll.INSTANCE.windowsfont(400, 200, 48, 0, 3, 1, "arial", "DEG 0");
            TscLibDll.INSTANCE.windowsfont(400, 200, 48, 90, 3, 1, "arial", "DEG 90");
            TscLibDll.INSTANCE.windowsfont(400, 200, 48, 180, 3, 1, "arial", "DEG 180");
            TscLibDll.INSTANCE.windowsfont(400, 200, 48, 270, 3, 1, "arial", "DEG 270");
            TscLibDll.INSTANCE.printlabel("1", "1");
            TscLibDll.INSTANCE.closeport();
        }
    
    }
```

# 4.总结

佳博官方的API写的太烂了，什么都没有描述，demo下载下来运行全是错，在网上找的资料完全不够用，我基本上走了所有的坑，为了让和我相同情况的码农朋友们能在这条路上走的轻松，特意整理了一下这篇文章，如果文章内容描述有误，请在评论处提出，如果遇到了我没有描述的其他问题，请联系QQ1010830256联系我我们一起探讨，QQ偶尔上线，还望谅解。